document.addEventListener("DOMContentLoaded", trendsUpload);

function trendsUpload() {

    let urlSearch = "https://api.giphy.com/v1/gifs/trending?api_key=rKzZ0CUGotLBn5mkZmbFk5Icb4qx57AR&limit=24&rating=G";

    // console.log(urlSearch);
    fetch(urlSearch)
        .then(response => response.json())
        .then(content => {
            // debugger;
            // console.log(content.data)
            // console.log("META", content.meta);
            // crea campos html

            var contenedorTendencias = document.getElementById("tendencias-gif");
            if (!contenedorTendencias) { return }
            contenedorTendencias.innerHTML = "";
            for (let index = 0; index < content.data.length; index++) {

                var gifs = document.createElement("img");
                var divs = document.createElement("div");
                var contenedorgifs = document.createElement("div");
                var titleGifs = document.createElement("p");
                // console.log('title',content.data[index].title)
                titleGifs.innerHTML = content.data[index].title;
                gifs.src = content.data[index].images.fixed_height_downsampled.url;
                contenedorTendencias.appendChild(contenedorgifs).appendChild(divs).appendChild(titleGifs).after(gifs);
                gifs.setAttribute("id", "gifs-id");
                divs.setAttribute("id", "div-gifs");
                titleGifs.setAttribute("id", "p-gifs");
                contenedorgifs.setAttribute("id", "contenedorgifs");
                contenedorgifs.setAttribute("width", "268px");
                contenedorgifs.setAttribute("height", "278px");
            }

            let tendenciasGif = document.querySelector(".tendencias-gif");
            // tendenciasGif.insertAdjacentElement("afterbegin", fig);
            document.querySelector("#search-text").value = "";
        })
        .catch(err => {
            console.error(err);
        })
};