/**
 * It handles to loop data based on passed content
 * It's a global functions to handles the loop and error
 * @param {Object} content   - Api fatched data
 * @param {String} heading  - Heading 
 */
function loopData(content) {
    var contenedorTendencias = $("#tendencias-gif");
    contenedorTendencias.innerHTML = "";
    let index = 0,
        contentLength = content.data.length;
    for (index; index < contentLength; index++) {
        var gifs = document.createElement("img");
        var divs = document.createElement("div");
        var contenedorgifs = document.createElement("div");
        var titleGifs = document.createElement("p");
        titleGifs.innerHTML = content.data[index].title;
        gifs.src = content.data[index].images.fixed_height_downsampled.url;
        contenedorTendencias.appendChild(contenedorgifs).appendChild(divs).appendChild(titleGifs).after(gifs);
        gifs.setAttribute("id", "gifs-id");
        divs.setAttribute("id", "div-gifs");
        titleGifs.setAttribute("id", "p-gifs");
        contenedorgifs.setAttribute("id", "contenedorgifs");
        contenedorgifs.setAttribute("width", "268px");
        contenedorgifs.setAttribute("height", "278px");
    }

    let tendenciasGif = $(".tendencias-gif");
    // tendenciasGif.insertAdjacentElement("afterbegin", fig);
    $("#search-text").value = "";
    window.scrollTo(0, 720);
}

/**
 * It handles all the error from API endpoint
 * I t just print outs the error message in console
 * @param {Object} err   -Error object content
 * 
 * @return {Void}  - It not gonna return anything
 */
function onErrorSuggestion(err) {
    console.error(err);
}