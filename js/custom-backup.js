var videoPlayer = document.getElementById('videoPlayer');
var image = document.getElementById('gif-image');
var capture = document.getElementById('capture-title');
var capturing = document.getElementById('capturing-title');
var preview = document.getElementById('preview-title');
var uploadTitle = document.getElementById('upload-title');


var startBtn = document.getElementById('btn-start-area');
var recordingBtn = document.getElementById('btn-recording-area');
var previewBtn = document.getElementById('btn-preview-area');
var uploadCancelBtn = document.getElementById('upload-cancel');

var timerView = document.getElementById('timerView');
var previewPlay = document.getElementById('previewPlay');
var uploading = document.getElementById('uploading');


// capturar camara
function captureCamera(callback) {
    navigator.mediaDevices.getUserMedia({
        audio: false,
        video: { facingMode: "user", width: 832, height: 434 }
    }).then(function(camera) {
        callback(camera);

    }).catch(function(error) {
        alert('Unable to capture your camera. Please check console logs.');
        console.error(error.name, error.message);
    });
}

// frenar camara
function stopCamera() {
    videoPlayer.src = videoPlayer.srcObject = null;

    recorder.camera.stop();
    recorder.destroy();
    recorder = null;
}

// vivo camara

function autoStart() {
    capture.classList.add("show");
    capturing.classList.add("hide");
    preview.classList.remove("show");
    preview.classList.add("hide");
    uploadTitle.classList.remove("show");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.add("show");
    image.classList.add("hide");
    image.classList.remove("show");

    startBtn.classList.remove("hide");
    recordingBtn.classList.add("hide");
    previewBtn.classList.add("hide");


    timerView.classList.remove("show");
    timerView.classList.add("hide");

    previewPlay.classList.remove("show");
    previewPlay.classList.add("hide");

    uploading.classList.remove("show");
    uploading.classList.add("hide");

    uploadCancelBtn.classList.remove("show");
    uploadCancelBtn.classList.add("hide");


    captureCamera(function(camera) {

        clearTimer()
        videoPlayer.srcObject = camera;
        recorder = RecordRTC(camera, {
            type: 'video'
        });

        recorder.camera = camera;
    });
};


// Crear gif
var blob;
var recorder;


function stopRecordingCallback() {
    //document.querySelector('h1').innerHTML = 'Gif recording stopped: ' + bytesToSize(recorder.getBlob().size);
    image.src = URL.createObjectURL(recorder.getBlob());
    // console.log('url now',image.src);
    // blob = image.src;
    // blob = recorder instanceof Blob ? recorder : recorder.blob;
    // console.log('blob',blob);
    // console.log('upload blob type',blob.type);
    recorder.camera.stop();
    // recorder.destroy();
    // recorder = null;

}

var recorder; // globally accessible
var imgUrl;
document.getElementById('btn-start-recording').onclick = function() {
    stopCamera()
        // release camera
        // recorder.camera = camera;

    capture.classList.remove("show");
    capture.classList.add("hide");
    capturing.classList.add("show");
    preview.classList.add("hide");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.remove("show");
    videoPlayer.classList.add("hide");
    image.classList.remove("hide");
    image.classList.add("show");


    startBtn.classList.add("hide");
    recordingBtn.classList.add("show");
    previewBtn.classList.add("hide");

    timerView.classList.remove("hide");

    previewPlay.classList.add("hide");
    uploading.classList.add("hide");
    uploadCancelBtn.classList.add("hide");

    // this.disabled = true;

    // console.log(startBtn);

    captureCamera(function(camera) {
        timerStart();
        recorder = RecordRTC(camera, {
            type: 'gif',
            quality: 10,
            height: 434,
            width: 832,
            hidden: 240,

            onGifPreview: function(gifURL) {
                image.src = gifURL;
                imgUrl = image.src;
                // var image = document.getElementById('image')
                //     image.src = imgUrl;
            }
        });

        recorder.startRecording();


        // release camera on stopRecording
        recorder.camera = camera;

        //document.getElementById('btn-stop-recording').disabled = false;
    });
};

document.getElementById('btn-stop-recording').onclick = function() {
    timerStop();
    this.disabled = true;

    recorder.stopRecording(stopRecordingCallback);
    previewGif();
};





// Preview del gif

function previewGif() {

    capture.classList.add("hide");
    capturing.classList.remove("show");
    capturing.classList.add("hide");
    preview.classList.add("show");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.add("hide");

    startBtn.classList.add("hide");
    recordingBtn.classList.remove("show");
    recordingBtn.classList.add("hide");
    previewBtn.classList.remove("hide");

    timerView.classList.add("show");

    previewPlay.classList.remove("hide");
    previewPlay.classList.add("show");

    uploading.classList.add("hide");



}

// subir gif

function uploadGif() {
    // console.log("asdasd");

    capture.classList.add("hide");
    capturing.classList.add("hide");
    preview.classList.remove("show");
    preview.classList.add("hide");
    uploadTitle.classList.remove("hide");
    uploadTitle.classList.add("show");

    videoPlayer.classList.add("hide");
    image.classList.remove("show");
    image.classList.add("hide");


    startBtn.classList.add("hide");
    recordingBtn.classList.remove("show");
    recordingBtn.classList.add("hide");
    previewBtn.classList.add("hide");

    timerView.classList.remove("show");
    timerView.classList.add("hide");

    previewPlay.classList.remove("show");
    previewPlay.classList.add("hide");

    uploading.classList.remove("hide");
    uploading.classList.add("show");
    uploadCancelBtn.classList.remove("hide");
    uploadCancelBtn.classList.add("show");
    clearTimer();
    saveLocal();
}



// guardando gif en localstorage

function saveLocal() {

    var blob = recorder instanceof Blob ? recorder : recorder.blob;
    console.log('blob size', blob.size)
    if (blob.size > 5242880) {
        console.log("archivo pesado")
        window.alert("archivo muy pesado. Intenta Nuevamente.")
        window.reload();
        return;
    }


    var fileType = blob.type.split('/')[0];

    var fileName = (Math.random() * 1000).toString().replace('.', '');

    fileName += '.gif';


    let fileReader = new FileReader()

    fileReader.onload = function() {
        localStorageSpace();
        if (typeof(Storage) !== "undefined") {
            if (this.result && localStorage) {
                // My code here
                var myLocalFiles = [];
                var store = localStorage.getItem('gifFiles');
                if (store) {
                    myLocalFiles = JSON.parse(store);
                    myLocalFiles.push(this.result);
                } else {
                    myLocalFiles = [this.result];
                }
                myLocalFiles = JSON.stringify(myLocalFiles);
                localStorage.setItem('gifFiles', myLocalFiles);
                // end my code 
                // window.localStorage.setItem(fileName, this.result)
            } else {
                console.log('gif-image saving failed or browser not support')
            }
        } else {
            console.log("Sorry, your browser does not support web storage...")
        }
    }
    fileReader.readAsDataURL(blob);

    setTimeout(function() {
        document.getElementById("preRecordScreen").classList.remove("show-bl");
        document.getElementById("preRecordScreen").classList.add("hide");

        // document.getElementById("uploadingScreen").classList.add("hide");
        document.getElementById("afterRecordPreviewScreen").classList.remove("hide");
        document.getElementById("afterRecordPreviewScreen").classList.add("show-bl");
        showRecodedGif();
    }, 2000)


    function localStorageSpace() {
        var data = '';

        for (var key in window.localStorage) {

            if (window.localStorage.hasOwnProperty(key)) {
                data += window.localStorage[key];

            }
        }

        var spaceRemaining = ((5120 - ((data.length * 16) / (8 * 1024)).toFixed(2)));
        var fileSize = (((blob.size) / (1024)).toFixed(2));
        // console.log(fileSize, spaceRemaining);
        if (fileSize > spaceRemaining) {
            console.log('Out of space');
            return;
        }
    }
}



// obtener gif del local storage



// cronometro

var seconds = 0,
    minutes = 0,
    hours = 0,
    t;

function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    timerView.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    timerStart();
}

function timerStart() {
    t = setTimeout(add, 1000);
}

function timerStop() {
    clearTimeout(t);
}

function clearTimer() {
    timerView.textContent = "00:00:00";
    seconds = 0;
    minutes = 0;
    hours = 0;
}

function showRecodedGif() {
    var createdGif = document.getElementById('created-gif');
    var downloadImg = document.getElementById('downloadImg');

    if (typeof(Storage) !== "undefined") {
        if (localStorage) {
            // var gifName = localStorage.key(0)
            // createdGif.src = gif;
            var gifImges = localStorage.getItem('gifFiles');
            gifImges = JSON.parse(gifImges);
            var lastRecordedGif = gifImges[gifImges.length - 1];

            createdGif.setAttribute("src", lastRecordedGif);
            downloadImg.setAttribute('href', lastRecordedGif);
            downloadImg.setAttribute('download', lastRecordedGif);
            //looping to html elements
            viewRecordedGifs(gifImges);

        }
    } else {
        console.log("Sorry, your browser does not support web storage...");
    }
}


// Subiendo gif en html

function viewRecordedGifs(data) {
    // console.log(content.data)
    // console.log("META", content.meta);
    // crea campos html

    var contenedorTendencias = document.getElementById("viewRecorded");
    contenedorTendencias.innerHTML = "";
    // debugger;
    // return;
    var index = 0;
    var itemLength = data.length;
    for (index; index < itemLength; index++) {

        var gifs = document.createElement("img");
        var divs = document.createElement("div");
        var contenedorgifs = document.createElement("div");
        // var titleGifs = document.createElement("p");
        // console.log('title',content.data[index].title)
        // titleGifs.innerHTML = data[index].title;
        gifs.src = data[index];
        contenedorTendencias.appendChild(contenedorgifs).appendChild(divs).appendChild(gifs);
        gifs.setAttribute("id", "gifs-id");
        divs.setAttribute("id", "div-gifs");
        // titleGifs.setAttribute("id", "p-gifs");
        contenedorgifs.setAttribute("id", "gif-images");
        contenedorgifs.setAttribute("width", "268px");
        contenedorgifs.setAttribute("height", "278px");
    }

    let tendenciasGif = document.querySelector(".tendencias-gif");
    tendenciasGif.insertAdjacentElement("afterbegin", fig);
    document.querySelector("#search-text").value = "";
};


//boton comenzar


$("#start").addEventListener('click', function() {
    $("#capture-step01").classList.add('hide');
    $("#preRecordScreen").classList.remove('hide');
    $("#preRecordScreen").classList.add('show-bl');
    autoStart();
});