/**
 * It shows the today's suggestion GIF images
 * Here script listen the click events for suggestion box clicks
 * On every click it gets the data from API based on the category 
 * category - Each category gets from clicked button data attributes
 */

//  Loading script after DOM loaded

(function() {
    var allSuggestionButtons = $$(".contenedor-gifs-sugerencias .button-sugerencias-1");
    allSuggestionButtons.forEach(function(sButton) {
        sButton.addEventListener("click", function(e) {
            e.preventDefault();
            var
                queryString = this.getAttribute('data-query');
            let url = SEARCH_API + "&q=" + queryString;
            url = url.concat("=&limit=24");
            document.getElementById("tendencias-resultados").innerHTML = " " + queryString + " [resultados]";
            fetch(url)
                .then(response => response.json())
                .then(function(content) { loopData(content); })
                .catch(onErrorSuggestion);
        });
    });
})();