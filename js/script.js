//                     PRIMERA PARTE


//  API KEY rKzZ0CUGotLBn5mkZmbFk5Icb4qx57AR
// LLAMADO A LA API SEARCH

/**
 * All the API and  keys
 */

const APIKEY = "rKzZ0CUGotLBn5mkZmbFk5Icb4qx57AR";
const SEARCH_API = "https://api.giphy.com/v1/gifs/search?api_key=" + APIKEY;
const UPLOAD_API = "https://upload.giphy.com/v1/gifs?api_key=" + APIKEY;

// Element selector shortcut
const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

var themeType = "day";

/**
 * Setup on init applicaiton
 */
document.addEventListener("DOMContentLoaded", app);

function app() {
    var finderBtn = $("#finder");
    if (finderBtn) {
        $("#finder").addEventListener("click", function(ev) {
            ev.preventDefault();
            console.log("clicked");
            let str = document.getElementById("search-text").value.trim();
            let url = SEARCH_API + "&q=" + str;
            url = url.concat("=&limit=24");
            fetch(url)
                .then(response => response.json())
                .then(function(content) { loopData(content); })
                .catch(onErrorSuggestion);
        });
    }
}

//Cambiar texto de "tendencias" a "resultados"

function terminoResultado() {
    let busquedaSearch = document.getElementById("search-text").value.trim();
    document.getElementById("tendencias-resultados").innerHTML = " " + busquedaSearch + " [resultados]";
};


//Scroll de search a resultados


function scrollWin() {
    window.scrollTo(0, 720);
}

// Show/hide on click search and outside of it
document.addEventListener("click", (evt) => {
    const elementoClickeado = document.getElementById("search-text");
    if (!elementoClickeado) { return }
    var targetElement = evt.target; // elemento clickeado
    if (targetElement == elementoClickeado) {
        // console.log("Theme type", themeType);
        // Esto es un click que da devolución
        document.getElementById("sugerencias").style.display = "block";
        if (themeType === 'night') {
            document.getElementById("finder").style.background = "#ee3efe";
            var img = document.getElementById("lupa");
            img.setAttribute("src", "./img/lupa_light.svg");
            document.getElementById("p-finder").style.color = "#ffffff";
        } else {
            document.getElementById("finder").style.background = "#F7C9F3";
            document.getElementById("finder").style["boxShadow"] = "inset -1px -1px 0 0 #997D97, inset 1px 1px 0 0 #FFFFFF";
            document.getElementById("p-finder").style.color = "#110038";
            var img = document.getElementById("lupa");
            img.setAttribute("src", "./img/lupa.svg");
        }


        return;
    }

    // Esto es cuando clickea afuera
    document.getElementById("sugerencias").style.display = "none";
    document.getElementById("finder").style.background = "#E6E6E6";
    document.getElementById("finder").style["boxShadow"] = "inset -1px -1px 0 0 #B4B4B4, inset 1px 1px 0 0 #FFFFFF";
    document.getElementById("p-finder").style.color = "#B4B4B4";
    var img = document.getElementById("lupa");
    img.setAttribute("src", "./img/lupa_inactive.svg");
});

/**
 * Adding events for CAT,DOG and ANIMAL button
 */
document.addEventListener("DOMContentLoaded", function() {
    var filterButtons = $$("#sugerencias div");
    filterButtons.forEach(function(flButton) {
        flButton.addEventListener("click", function(e) {
            e.preventDefault();
            var queryString = this.getAttribute('data-query');
            getSuggestionFilterContent(queryString);
            scrollWin();
        });
    });
});

function getSuggestionFilterContent(queryString) {
    let url = SEARCH_API + "&q=" + queryString;
    url = url.concat("=&limit=24");
    $("#tendencias-resultados").innerHTML = " " + queryString + " [resultados]";
    fetch(url)
        .then(response => response.json())
        .then(function(content) { loopData(content) })
        .catch(onErrorSuggestion)
}


// CAMBIO DE THEME 


document.addEventListener("click", (evt) => {
    const themeClickeado = document.getElementById("seleccion-temas");
    if (!themeClickeado) { return }
    let targetElement = evt.target; // elemento clickeado

    do {
        if (targetElement == themeClickeado) {
            // Esto es un click que da devolución
            document.getElementById("temas-despegable").style.display = "block";
            // document.getElementById("finder").style.background = "#F7C9F3";
            // document.getElementById("finder").style["boxShadow"] = "inset -1px -1px 0 0 #997D97, inset 1px 1px 0 0 #FFFFFF";
            // document.getElementById("p-finder").style.color = "#110038";
            // var img = document.getElementById("lupa");
            // img.setAttribute("src", "/img/lupa.svg");
            return;
        }
        //Va arriba
        targetElement = targetElement.parentNode;
    } while (targetElement);

    // Esto es cuando clickea afuera
    document.getElementById("temas-despegable").style.display = "none";
    // document.getElementById("finder").style.background = "#E6E6E6";
    // document.getElementById("finder").style["boxShadow"] = "inset -1px -1px 0 0 #B4B4B4, inset 1px 1px 0 0 #FFFFFF";
    // document.getElementById("p-finder").style.color = "#B4B4B4";
    // var img = document.getElementById("lupa");
    // img.setAttribute("src", "/img/lupa_inactive.svg");
});

// DESPLEGABLE SAILOR DAY TO SAILOR NIGHT
var nightBtn = $("#sailor-night");
if (nightBtn) {
    nightBtn.addEventListener("click", function(e) {
        e.preventDefault();
        document.getElementById("sailor-night").style.background = "#2E32FB";
        document.getElementById("sailor-night").style.border = "1px solid rgba(51,53,143,0.20)";
        document.getElementById("sailor-night").style["boxShadow"] = "inset -1px -1px 0 0 #E6DCE4, inset 1px 1px 0 0 #FFFFFF";
        document.getElementById("sailor-night").style.color = "white";
        document.getElementById("sailor-day").style.background = "#CCCCCC";
        document.getElementById("sailor-day").style.border = "1px solid #808080";
        document.getElementById("sailor-day").style["boxShadow"] = "inset -1px -1px 0 0 #B4B4B4, inset 1px 1px 0 0 #FFFFFF";
    });
}


// DESPLEGABLE SAILOR NIGHT TO SAILOR DAY
var dayBtn = $("#sailor-day");
if (dayBtn) {
    dayBtn.addEventListener("click", function() {
        document.getElementById("sailor-day").style.background = "#FFF4FD";
        document.getElementById("sailor-day").style.border = "1px solid #CCA6C9";
        document.getElementById("sailor-day").style["boxShadow"] = "inset -1px -1px 0 0 #E6DCE4, inset 1px 1px 0 0 #FFFFFF";
        document.getElementById("sailor-night").style.background = "#F0F0F0";
        document.getElementById("sailor-night").style.border = "1px solid #808080";
        document.getElementById("sailor-night").style["boxShadow"] = "inset -1px -1px 0 0 #B4B4B4, inset 1px 1px 0 0 #FFFFFF";
        document.getElementById("sailor-night").style.color = "#110038";
    });
}




// CAMBIAR A CSS REGULAR

function swapStyleSheetDay(sheet) {
    // console.log('sheet=',sheet);
    document.getElementById('pagestyle').setAttribute('href', sheet);
    var logo1 = document.getElementById("logo");
    logo1.setAttribute("src", "./img/gifOF_logo.png");
    var dropdown1 = document.getElementById("dropdown");
    if (dropdown1) {
        dropdown1.setAttribute("src", "./img/dropdown.svg");
    }

};
//CAMBIAR A CSS NIGHT

function swapStyleSheetNight(sheet) {
    // console.log('sheet=',sheet);
    document.getElementById('pagestyle').setAttribute('href', sheet);
    var logo2 = document.getElementById("logo");
    logo2.setAttribute("src", "./img/gifOF_logo_dark.png");
    var dropdown2 = document.getElementById("dropdown");
    if (dropdown2) {
        dropdown2.setAttribute("src", "./img/forward.svg");
        dropdown2.setAttribute("fill", "white");
    }
};



function theamColor(time) {

    var day = 'css/style-regular.css';
    var night = 'css/style-night.css';


    if (typeof(Storage) !== "undefined") {
        if (localStorage.theamColor) {
            localStorage.theamColor = time;
            if (time == 'day') {
                swapStyleSheetDay(day);
                themeType = "day";
            } else {
                swapStyleSheetNight(night);
                themeType = "night";
            }
        } else {
            localStorage.theamColor = 'day';
            swapStyleSheetDay(day);
            themeType = "day";
        }
    } else {
        console.log("Su navegador no soporta webstorage...");
    }

}
theamColor(localStorage.theamColor);