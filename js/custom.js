var videoPlayer = $('#videoPlayer');
var image = $('#gif-image');
var capture = $('#capture-title');
var capturing = $('#capturing-title');
var preview = $('#preview-title');
var uploadTitle = $('#upload-title');


var startBtn = $('#btn-start-area');
var recordingBtn = $('#btn-recording-area');
var previewBtn = $('#btn-preview-area');
var uploadCancelBtn = $('#upload-cancel');

var timerView = $('#timerView');
var previewPlay = $('#previewPlay');
var uploading = $('#uploading');


// capturar camara
function captureCamera(callback) {
    navigator.mediaDevices.getUserMedia({
        audio: false,
        video: { facingMode: "user", width: 832, height: 434 }
    }).then(function(camera) {
        callback(camera);

    }).catch(function(error) {
        alert('Unable to capture your camera. Please check console logs.');
        console.error(error.name, error.message);
    });
}

// frenar camara
function stopCamera() {
    videoPlayer.src = videoPlayer.srcObject = null;

    recorder.camera.stop();
    recorder.destroy();
    recorder = null;
}

// vivo camara

function autoStart() {
    capture.classList.add("show");
    capturing.classList.add("hide");
    preview.classList.remove("show");
    preview.classList.add("hide");
    uploadTitle.classList.remove("show");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.add("show");
    image.classList.add("hide");
    image.classList.remove("show");

    startBtn.classList.remove("hide");
    recordingBtn.classList.add("hide");
    previewBtn.classList.add("hide");


    timerView.classList.remove("show");
    timerView.classList.add("hide");

    previewPlay.classList.remove("show");
    previewPlay.classList.add("hide");

    uploading.classList.remove("show");
    uploading.classList.add("hide");

    uploadCancelBtn.classList.remove("show");
    uploadCancelBtn.classList.add("hide");


    captureCamera(function(camera) {

        clearTimer()
        videoPlayer.srcObject = camera;
        recorder = RecordRTC(camera, {
            type: 'video'
        });

        recorder.camera = camera;
    });
};


// Crear gif
var blob;
var recorder;


function stopRecordingCallback() {
    //document.querySelector('h1').innerHTML = 'Gif recording stopped: ' + bytesToSize(recorder.getBlob().size);
    image.src = URL.createObjectURL(recorder.getBlob());
    // console.log('url now',image.src);
    // blob = image.src;
    // blob = recorder instanceof Blob ? recorder : recorder.blob;
    // console.log('blob',blob);
    // console.log('upload blob type',blob.type);
    recorder.camera.stop();
    // recorder.destroy();
    // recorder = null;

}

var recorder; // globally accessible
var imgUrl;
$('#btn-start-recording').onclick = function() {
    stopCamera()
        // release camera
        // recorder.camera = camera;

    capture.classList.remove("show");
    capture.classList.add("hide");
    capturing.classList.add("show");
    preview.classList.add("hide");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.remove("show");
    videoPlayer.classList.add("hide");
    image.classList.remove("hide");
    image.classList.add("show");


    startBtn.classList.add("hide");
    recordingBtn.classList.add("show");
    previewBtn.classList.add("hide");

    timerView.classList.remove("hide");

    previewPlay.classList.add("hide");
    uploading.classList.add("hide");
    uploadCancelBtn.classList.add("hide");

    // this.disabled = true;

    // console.log(startBtn);

    captureCamera(function(camera) {
        timerStart();
        recorder = RecordRTC(camera, {
            type: 'gif',
            quality: 10,
            height: 434,
            width: 832,
            hidden: 240,

            onGifPreview: function(gifURL) {
                image.src = gifURL;
                imgUrl = image.src;
                // var image = $('image')
                //     image.src = imgUrl;
            }
        });

        recorder.startRecording();


        // release camera on stopRecording
        recorder.camera = camera;

        //$('btn-stop-recording').disabled = false;
    });
};

$('#btn-stop-recording').onclick = function() {
    timerStop();
    this.disabled = true;

    recorder.stopRecording(stopRecordingCallback);
    previewGif();
};





// Preview del gif

function previewGif() {

    capture.classList.add("hide");
    capturing.classList.remove("show");
    capturing.classList.add("hide");
    preview.classList.add("show");
    uploadTitle.classList.add("hide");

    videoPlayer.classList.add("hide");

    startBtn.classList.add("hide");
    recordingBtn.classList.remove("show");
    recordingBtn.classList.add("hide");
    previewBtn.classList.remove("hide");

    timerView.classList.add("show");

    previewPlay.classList.remove("hide");
    previewPlay.classList.add("show");

    uploading.classList.add("hide");



}

// Generic functions
async function fetchURL(url, params = null) {
    try {
        const fetchData = await fetch(url, params);
        const response = await fetchData.json();
        return response;
    } catch (error) {
        if (error.name !== "AbortError") {
            console.log("Error al obtener resultados");
        }
        return error;
    }
}

async function submitToApi() {
    const formData = new FormData();
    formData.append("file", recorder.blob, "myGif.gif");
    const params = {
        method: "POST",
        body: formData,
        json: true
    };
    const data = await fetchURL(UPLOAD_API, params);
    console.log(await data);
    return await data;
}

// Saving to localstorage
async function saveGifToLocalStorage(gif) {
    const response = await fetchURL(`https://api.giphy.com/v1/gifs/${gif}?api_key=${APIKEY}`);
    const data = response.data;
    const gifID = data.id;
    // data.embed_url - contains the image url
    // const stringifiedData = JSON.stringify(data);
    var files = [];
    var localFiles = localStorage.getItem(`gif-files`);
    if (localFiles) {
        files = JSON.parse(localFiles);
    }
    files.push(data);
    localStorage.setItem(`gif-files`, JSON.stringify(files));
    return data;
}

function isNotEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) return true;
    }
    return false;
}

function loadMyGifs(myGifs) {
    console.log("From local gif list", myGifs);
    // for (let myGifKey in myGifs) {
    //     const parsedGifData = JSON.parse(myGifs[myGifKey]);
    //     let aspectRatio = "";
    //     parsedGifData.images["480w_still"].width / parsedGifData.images["480w_still"].height >= 1.5 ?
    //         (aspectRatio = "item-double") :
    //         null;
    //     $gifsGrid.append(newElement("myGif", parsedGifData, aspectRatio));
    // }
}


function showNewCreatedGif() {
    // myGifs = {};

    let myGifs = localStorage.getItem('gif-files');
    if (isNotEmpty(myGifs)) {
        myGifs = JSON.parse(myGifs);
        var
            gifContainer = $("#viewRecorded ul"),
            i = 0,
            dataLength = myGifs.length;
        for (i; i < dataLength; i++) {
            // debugger;
            let li = document.createElement('li');
            let myImg = document.createElement('img');
            myImg.setAttribute('src', myGifs[i].images.original.url);
            myImg.style.cssText = 'width: auto';
            li.append(myImg);
            gifContainer.append(li);
        }
    }

}

// Show pre resored gifs 
showNewCreatedGif();

// Global set for current gif image link
var currentGifUrl = 'http://twitter666.com';
// subir gif
async function uploadGif() {

    capture.classList.add("hide");
    capturing.classList.add("hide");
    preview.classList.remove("show");
    preview.classList.add("hide");
    uploadTitle.classList.remove("hide");
    uploadTitle.classList.add("show");

    videoPlayer.classList.add("hide");
    image.classList.remove("show");
    image.classList.add("hide");


    startBtn.classList.add("hide");
    recordingBtn.classList.remove("show");
    recordingBtn.classList.add("hide");
    previewBtn.classList.add("hide");

    timerView.classList.remove("show");
    timerView.classList.add("hide");

    previewPlay.classList.remove("show");
    previewPlay.classList.add("hide");

    uploading.classList.remove("hide");
    uploading.classList.add("show");
    uploadCancelBtn.classList.remove("hide");
    uploadCancelBtn.classList.add("show");
    try {
        var mynewGif = await submitToApi();
        if ((await mynewGif.meta.status) === 200) {
            console.log("Upload success--");
            let newGifId = await mynewGif.data.id;
            var newImage = await saveGifToLocalStorage(await newGifId);
            $("#preRecordScreen").classList.remove("show-bl");
            $("#preRecordScreen").classList.add("hide");
            // $("uploadingScreen").classList.add("hide");
            $("#afterRecordPreviewScreen").classList.remove("hide");
            $("#afterRecordPreviewScreen").classList.add("show-bl");
            var createdGif = $('#created-gif');
            var downloadImg = $('#downloadImg');
            currentGifUrl = newImage.url; // storing current gif url
            var imageUrl = newImage.images.original.url;
            createdGif.setAttribute("src", imageUrl);
            downloadImg.setAttribute('href', imageUrl);
            // downloadImg.setAttribute('download', 'mygif.gif');
            downloadImg.addEventListener("click", function(e) {
                e.preventDefault();
                downloadImage(newImage.slug, newImage.images.original.url);
            })
            showNewCreatedGif();
        } else {
            console.log("Somehting wrong! on upload")
                // showElements($stage7);
                // hideElements($stage5);
                // uploadLoadingBar.reset();
                // $errorMsg.innerText = `${e.name}\n${e.message}`;
        }
    } catch (e) {

    }

    clearTimer();
    // saveLocal();
}


function downloadImage(name, imageUrl) {
    var x = new XMLHttpRequest();
    x.open("GET", imageUrl, true);
    x.responseType = 'blob';
    x.onload = function(e) { download(x.response, name + ".gif", "image/gif"); }
    x.send();
}


function copyCreatedGifLink() {
    var field = $("#gifLinkElement");
    field.select();
    field.setSelectionRange(0, 99999); /*For mobile devices*/
    document.execCommand("copy");
    $("#popup-window").style.display = 'none';
}
$("#copyGifBtn").addEventListener("click", function(e) {
    e.preventDefault();
    var textarea = document.createElement('textarea');
    textarea.setAttribute('id', 'gifLinkElement');
    $('body').append(textarea);
    $("#gifLinkElement").innerHTML = currentGifUrl;
    $("#popup-message").innerHTML = `Se ha copiado el enlace ${currentGifUrl} al portapapeles`;
    $("#popup-window").style.display = 'flex';
});


// obtener gif del local storage
// Closing popup window
$("#popup-close").addEventListener('click', function(e) {
    e.preventDefault();
    $("#popup-window").style.display = 'none';
});
// closing popup after copied link
$("#popup-primary").addEventListener('click', function(e) {
    e.preventDefault();
    copyCreatedGifLink();
});

// cronometro

var seconds = 0,
    minutes = 0,
    hours = 0,
    t;

function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    timerView.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    timerStart();
}

function timerStart() {
    t = setTimeout(add, 1000);
}

function timerStop() {
    clearTimeout(t);
}

function clearTimer() {
    timerView.textContent = "00:00:00";
    seconds = 0;
    minutes = 0;
    hours = 0;
}



// Subiendo gif en html

function viewRecordedGifs(data) {
    // console.log(content.data)
    // console.log("META", content.meta);
    // crea campos html

    var contenedorTendencias = $("#viewRecorded");
    contenedorTendencias.innerHTML = "";
    // debugger;
    // return;
    var index = 0;
    var itemLength = data.length;
    for (index; index < itemLength; index++) {

        var gifs = document.createElement("img");
        var divs = document.createElement("div");
        var contenedorgifs = document.createElement("div");
        // var titleGifs = document.createElement("p");
        // console.log('title',content.data[index].title)
        // titleGifs.innerHTML = data[index].title;
        gifs.src = data[index];
        contenedorTendencias.appendChild(contenedorgifs).appendChild(divs).appendChild(gifs);
        gifs.setAttribute("id", "gifs-id");
        divs.setAttribute("id", "div-gifs");
        // titleGifs.setAttribute("id", "p-gifs");
        contenedorgifs.setAttribute("id", "gif-images");
        contenedorgifs.setAttribute("width", "268px");
        contenedorgifs.setAttribute("height", "278px");
    }

    let tendenciasGif = $(".tendencias-gif");
    tendenciasGif.insertAdjacentElement("afterbegin", fig);
    $("#search-text").value = "";
};


//boton comenzar


$("#start").addEventListener('click', function() {
    $("#capture-step01").classList.add('hide');
    $("#preRecordScreen").classList.remove('hide');
    $("#preRecordScreen").classList.add('show-bl');
    autoStart();
});